def solution(a = [-1,3,-4,5,1,-6,2,1])
	i = 1
	result = []
	a_size = a.size

	result << 0 if a[1..a_size].reduce(:+) == 0

	while i < a_size do	
		x = a[0..i - 1].reduce(:+)
		y = a[i + 1..-1].reduce(:+)
		
		result << i if x == y

		i += 1
	end

	result << a_size - 1 if a[0..-2].reduce(:+) == 0

	return result unless result.empty?
	-1
end
